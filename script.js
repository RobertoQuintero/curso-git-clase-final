const c = document.getElementById('myContent')
const b = document.getElementById('btnLoad')
const l = document.getElementById('loading')

l.style.display='none'
b.addEventListener('click',evt=>{
  l.style.display= 'block'
  const xhr = new XMLHttpRequest()
  
  xhr.open('GET','/jason.json',true)
  
  xhr.addEventListener('load',e=>{
    setTimeout(() => {
      l.style.display='none'
      
    }, 2000);
    console.log(e.target.status)
    // console.log(e.target.responseText)
    const data = JSON.parse(e.target.responseText) 
    // console.log(data)
    draw(data)
  })
  xhr.send()
})
 
const draw = data =>{
  c.innerHTML=''
  const fragment = document.createDocumentFragment()

  data.forEach(notice => {
    const container = document.createElement('div')

    container.innerHTML=` <h2>${notice.Titulo}</h2>
    <p>${notice.Contenido}</p>
    <span>${notice.Fecha}</span>`

    fragment.appendChild(container)
  });
  c.appendChild(fragment)
}