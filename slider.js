const sliderMedia = window.matchMedia('(min-width:600px)')
const mainContainer = document.getElementById('mainContainer'),
  slider = document.getElementById('slider'),
  xhr = new XMLHttpRequest()
  xhr.open('GET','jason.json',true)
  xhr.addEventListener('load',e=>{
    const data = JSON.parse(e.target.responseText)
    draw(data)
    const sliderLength = slider.children.length
      let width= mainContainer.getBoundingClientRect().width,
        item = Math.floor(slider.querySelector('.dataItem').getBoundingClientRect().width)
      slider.firstElementChild.before(slider.lastElementChild)
      slider.style.transform= `translateX(-${item}px)`
      // slider.style.left=`-${item*2}px`
      slider.firstElementChild.before(slider.lastElementChild)
      sliderMatches(sliderMedia,sliderLength,width)
    console.log(width)
    
    const move = setInterval(() => {
      width= mainContainer.getBoundingClientRect().width
      sliderMatches(sliderMedia,sliderLength,width)
    }, 5000);
  })
  xhr.send()

const draw = data =>{
  const fragment = document.createDocumentFragment()
  data.forEach(notice => {
    const container = document.createElement('div')
    container.className= 'dataItem'
    container.innerHTML=` <div class="dataItem-info">
      <h2 class="dataItem-title">${notice.Titulo}</h2>
      <p class="dataItem-text">${notice.Contenido}</p>
      <span class="dataItem-date">${notice.Fecha}</span>
    </div>`

    fragment.appendChild(container)
  });
  slider.appendChild(fragment)
}
const moverDerecha =(width)=>{
  slider.style.transform= `translateX(-${width*2}px)`
  slider.classList.add('animation')
  slider.append(slider.firstElementChild)
  slider.style.transform= `translateX(-${width}px)`  
  
}
slider.addEventListener('animationend',() => slider.classList.remove('animation'))

const sliderMatches = (sliderMedia,sliderLength,width)=>{
  if(sliderMedia.matches){
    moverDerecha(width)
    slider.style.width= `${sliderLength/2}00%`
    slider.append(slider.firstElementChild)
    
  }else{
    
    slider.style.width= `${sliderLength}00%`
    moverDerecha(width)
  }
}
sliderMedia.addListener(sliderMatches)
