const c = document.getElementById('container'),
  xhr = new XMLHttpRequest(),
  row = document.getElementById('row')



xhr.open('GET','jason.json',true)
xhr.addEventListener('load',e=>{
  const data = JSON.parse(e.target.responseText)
  let i =0,j=i+1
  draw(data[i])
  const initialHeight= Math.ceil(c.getBoundingClientRect().height)
  row.style.minHeight=`${initialHeight}px`
  row.style.border= '1px solid red'
  const search = setInterval(() => {
    if(j<data.length){
      delay(data,j)
      j++
    }
    else{
      j=0
      delay(data,j)
  
      j++
    }
  }, 3000);
})
xhr.send()

const draw = data =>{
  c.innerHTML=''
  const container = document.createElement('div')
    container.innerHTML=` <h2>${data.Titulo}</h2>
    <p>${data.Contenido}</p>
    <span>${data.Fecha}</span>`
  c.appendChild(container)
}

const delay = (data,j)=>{
  c.style.opacity='0'
  setTimeout(() => {
    c.style.opacity = '1'
    draw(data[j])
  }, 500);
}
